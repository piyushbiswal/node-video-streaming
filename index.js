let express = require('express'),
    app = express(),
    bodyParser = require("body-parser");
    helmet=require("helmet"),
    cors = require("cors"),
    router = require("./routes");

// Secure express app by setting HTTP headers
app.use(helmet());
// allowing cross origin request
app.use(cors());
// Body-parser (To parse the request body)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
// setting Port 
app.set('Port', process.env.PORT || 7000);
app.use(express.static('public'));
/*
    Add to avoid cross origin access.
    Access-Control-Allow-Origin is set to '*' so that server REST APIs are accessible for all the domains.
    By setting domain name to some value, the API access can be restricted to only the mentioned domain.
    Eg, Access-Control-Allow-Origin: 'mywebsite.com'
*/
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "x-access-token,Content-Type");
    res.header("Access-Control-Expose-Headers", "x-access-token,Content-Type");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
    res.header("Content-Type",'application/json');
    next();
});

// router middleware
app.use("/",router)

// Error handling middleware
app.use("/",function(err,req,res){
    res.status(err.status || 404).json({success: false, error: {message: "URL not found"}})
 });


app.listen(app.get('Port'), function () {
    console.log("Application started on port " + app.get('Port'));
});
