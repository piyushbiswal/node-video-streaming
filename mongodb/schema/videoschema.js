"use strict";

module.exports=(mongoose)=>{
    let VideoSchema = new mongoose.Schema({
        createdby:{
            type:String
        },
        createdDate:{
            type:Date,
            default:new Date()
        },
        delete:{
            type:Boolean,
            default:false
        },
        deleteDate:{
            type:Date
        }
    },{strict:false})

    return mongoose.model("Video",VideoSchema)
}
