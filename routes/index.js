"use strict";

let express = require("express"),
    router = express.Router(),
    api = require("./api"),
    multer=require("multer"),
    multerUtils = require("../utils/multer");

router.post("/uploadVideo",multer(multerUtils.videoUpload).any(),api.video.uploadVideo);

router.get("/consumeVideo/:videoId",api.video.consumeVideo);

router.get("/listVideos",api.video.listVideos);


module.exports = router;