"use strict"
let path = require("path"),
    videoModel = require("../../mongodb").video;
let video = function () {
    this.uploadVideo = (req, res) => {
        let data = {
            createdby: "piyushbiswal",
            files: req.files
        }
        console.log(new Date() + " : <<<<<< files info>>>>>>>");
        console.log(data.files);
        let model = new videoModel(data);
        model.save().then((success) => {
            if (success) {
                res.json({ success: true });
            }
        })
            .catch((err) => {
                res.json({ success: false, error: err });
            })
    }

    this.consumeVideo = (req, res) => {
        let data = {
            id: req.videoId
        }
        videoModel.find(getQuery(data)).then((data) => {
            if (data) {
                res.json({ success: true, payload: data })
            }
        })
            .catch((err) => {
                res.json({ success: false, err: err })
            })
    }

    this.listVideos = (req, res) => {
        let data = {};
        videoModel.find(getQuery(data)).then((data) => {
            if (data) {
                console.log(data);
                res.json({ success: true, payload: data })
            }
        })
            .catch((err) => {

                res.json({ success: false, err: err })

            })
    }

    let getQuery = (data) => {
        let query = {
            delete: {
                $ne: true
            }
        }
        if (data.id) {
            query["_id"] = data.id;
        }
        return query;
    }
}

module.exports = {
    video: new video()
}
