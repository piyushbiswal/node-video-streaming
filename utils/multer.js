"use strict"

let multer = require("multer"),
    path = require("path"),
    GridFsStorage = require('multer-gridfs-storage'),
    Grid = require('gridfs-stream');

let storage = {
    destination: "public/videos",
    filename: (req, file, cb) => {
        console.log(file);
        cb(null,"xyz"+path.extname(file.originalname))
    }
}

let videoUpload = {
    storage: multer.diskStorage(storage)
}


module.exports = {
    videoUpload
}
